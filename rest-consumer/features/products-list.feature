Feature: Lista Productos
    Scenario: Cargar lista de Productos
        When we request the products list
        Then we should receive
            | nombre | descripcion |
            | Movil XL | Un teléfono grande con una de las mejores pantallas |
            | Movil Mini | Un teléfono mediano con una de las mejores cámaras |
            | Movil Standar | Un télefono estandar. Nada especial. |